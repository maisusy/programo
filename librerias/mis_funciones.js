function sumar(val_1, val_2){
    return val_1 + val_2;
  }
  
  function restar(val_1,val_2){
      return val_1 - val_2;
  }
  
  function multiplicar(val_1,val_2){
      return val_1 * val_2;
  }
  
  function dividir(val_1,val_2){
      return val_1 / val_2;
  }
  
  var msg_ok = "La operacion se realizo correctamente;"
  var msg_fail = "La operacion no se pudo completar;"
  exports.sumar = sumar;
  exports.restar = restar;
  exports.msg_ok = msg_ok;
  exports.msg_fail = msg_fail;
  exports.multiplicar = multiplicar;
  exports.dividir = dividir;